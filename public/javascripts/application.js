// Place your application-specific JavaScript functions and classes here
$(document).ready(function() {
	var oldpos = 0 ;
	$(".velgen-right").css({"height": $(window).height()-20, "overflow-y": "auto"}) ;
	$(".velgen-left").css("min-height", $(window).height()-20) ;
	/* This is basic - uses default settings */
	$(".single_image").fancybox({'cyclic':true, 'showNavArrows':true,'padding':'50'});
	$(".selected").parent().show() ;
	$(".velg-summary").first().toggleClass("active");
	$('.velg-summary').bind('click', function() { 
		var position = $(this).position().top;
		var rightpos = $('.velgen-right').position().top ;
		//alert(jump) ; 
		$(".active").toggleClass("active") ;
		$(this).toggleClass("active") ;  
    		$.ajax({
			  url: $(this).attr("id"),
			  success: function(data){
			    $(".velgen-right").html(data);

			    $(".single_image").fancybox({'cyclic':true, 'showNavArrows':true,'padding':'50'});
			  }
			});  
	});  
	/*$(".expandable").parent().mouseover(function(){
		$(this).children().show() ;
    	});
	$(".expandable").parent().mouseout(function(){
		if(!($(".expandable").parent().attr("class")=="selected")) {
	      		$(".expandable").hide() ;
		}
   	});*/

	$("#language-selector").mouseover(function(){
		$(".nf").toggleClass("ff") ;
    	});
	$("#language-selector").mouseout(function(){
		$(".nf").toggleClass("ff") ;
   	});

	/*$(window).scroll(function () {  
		var increment = $(document).scrollTop() ;
		$(".velgen-right").css("margin-top",increment) ;
	});*/
        $(window).scroll(function () { 
	var dif = $('.velgen-right').parent().offset().top-$(window).scrollTop() ;
	var bottomdif = parseInt($(".velgen-right").css('marginTop').replace(/[A-Za-z$-]/g, "")) + $(".velgen-right").height()-$(".velgen-left").height() ;
	//alert(bottomdif) ;
	if((bottomdif<0)||(oldpos - $(window).scrollTop() > 0)) 
	{
	if(dif<0)
	{
			var maxmove = $(".velgen-left").height() - $(".velgen-right").height() ;
			//alert(maxmove) ;
			var tomove = 5-dif ;
			//alert(5-dif) ;
			if(5-dif > maxmove)
			{
				tomove = maxmove ;
			}
			//alert(tomove) ;
			$('.velgen-right').stop().animate({
			     marginTop: tomove

			    }, "slow", function() {

			    });
	} else {
			$('.velgen-right').stop().animate({
			     marginTop: 8

			    }, "slow", function() {

			    });
	}		}
	oldpos = $(window).scrollTop() ;
        });
});
